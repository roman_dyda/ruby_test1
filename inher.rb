class A
  private
  def test2
    puts "Hi, I am test2 and I private"
  end

  protected
  def test1
    puts "Hi, I am test1 and I protected"
  end
end

class B < A
  def test3
    test1
  end

  def test4
    test2
  end
end


# class_a = A.new
# class_a.test1
# class_a.test2


# class_b = B.new
# class_b.test3
# class_b.test4
# class_c = C.new
# class_c.get_a_class
# class_b.test1
# class_b.test2
