class PrivateClass

  # def initialize(name="1234", surname=nil)
  #   self.name = name
  #   self.surname = surname
  # end
  #
  # def name
  #   puts @name
  # end
  # def surname
  #   puts @surname
  # end

  # private :come
  # protected :stay


  def go
    puts "I can go"
  end

  # def get_come
  #   come
  # end

  # def get_stay
  #   stay
  # end



  private
  def come
    puts "I can come"
  end


  protected
  def stay
    puts "I can stay"
  end
end


class ProtectedClass < PrivateClass
  def get_stay
    stay
  end
  def get_come
    come
  end
end


# private_class = PrivateClass.new
# private_class.go
# private_class.send("come")
# private_class.get_come
# private_class.send("stay")
# private_class.get_stay


protected_class = ProtectedClass.new
protected_class.get_stay
protected_class.send("come")

